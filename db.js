"use strict";
const Sequelize = require("sequelize");

const sequelize = new Sequelize(
  process.env.DATABASE_NAME,
  process.env.DATABASE_USERNAME,
  process.env.DATABASE_PASSWORd,
  {
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT,
    dialect: "mysql",
  }
);

try {
  sequelize.authenticate();
  console.log("Connection to Database.");
} catch (error) {
  console.error("Unable to connect to the database:", error);
}

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
// Models / tables

db.users = require("./modules/user/models/user.model")(sequelize, Sequelize);
db.blogs = require("./modules/blog/models/blog.model")(sequelize, Sequelize);
db.aboutbca = require("./modules/aboutbca/models/aboutbca.model")(
  sequelize,
  Sequelize
);
db.colleges = require("./modules/colleges/models/colleges.model")(
  sequelize,
  Sequelize
);
db.syllabus = require("./modules/syllabus/models/syllabus.model")(
  sequelize,
  Sequelize
);
db.slider = require("./modules/slider/models/slider.model")(
  sequelize,
  Sequelize
);

db.books = require("./modules/books/models/books.model")(sequelize, Sequelize);
db.notes = require("./modules/notes/models/notes.model")(sequelize, Sequelize);
db.question = require("./modules/oldquestion/models/oldquestion.model")(
  sequelize,
  Sequelize
);

// Relations
db.blogs.belongsTo(db.users);
db.users.hasMany(db.blogs);
db.aboutbca.belongsTo(db.users);
db.users.hasMany(db.aboutbca);
db.notes.belongsTo(db.users);
db.users.hasMany(db.notes);
db.question.belongsTo(db.users);
db.users.hasMany(db.question);
module.exports = db;
