const jwt = require("jsonwebtoken");
const config = process.env.JWT_secret;
const UserModel = require("./../db").users;

module.exports = function (req, res, next) {
  var token;
  if (req.headers["x-access-token"]) {
    token = req.headers["x-access-token"];
  }
  if (req.headers["authorization"]) {
    token = req.headers["authorization"];
  }
  if (req.headers["token"]) {
    token = req.headers["token"];
  }
  if (req.query.token) {
    token = req.query.token;
  }
  if (token) {
    jwt.verify(token, config, function (err, decoded) {
      if (err) {
        return next({ msg: "Error in Token Checking" });
      }
      UserModel.findByPk(decoded.id)
        .then(function (user) {
          if (user) {
            req.loggedInUser = user;
            return next();
          } else {
            return next({ msg: "User Remove from System." });
          }
        })
        .catch(function (err) {
          return next({ msg: "User Remove from System." });
        });
    });
  } else {
    next({
      msg: "Token Not Provided",
    });
  }
};
