const express = require("express");
require("dotenv").config();
const apiRoute = require("./routes/api.route");
const cors = require("cors");
const app = express();
const path = require("path");
const PORT = process.env.PORT || 5000;
require("./db");

// var corsOptions = {
// origin: "http://192.168.0.201:3000"
//  optionsSuccessStatus: 204,
// };
app.use(cors());
// ----------------------------------------------------
app.use(express.static("files"));
app.use(
  "/userimage",
  express.static(path.join(__dirname, "uploads/user_image"))
);
app.use(
  "/blogimage",
  express.static(path.join(__dirname, "uploads/blog_image"))
);
app.use(
  "/collegeimage",
  express.static(path.join(__dirname, "uploads/college_image"))
);
app.use("/syllabus", express.static(path.join(__dirname, "uploads/syllabus")));

app.use("/books", express.static(path.join(__dirname, "uploads/books")));
app.use("/notes", express.static(path.join(__dirname, "uploads/notes")));
app.use("/questions", express.static(path.join(__dirname, "uploads/question")));
app.use("/slider", express.static(path.join(__dirname, "uploads/slider")));

// --------------------------at last -------------------

app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());

//------------------------------------------------------
app.use("/api", apiRoute);

app.use(function (err, req, res, next) {
  res.status(err.status || 400).json({
    status: err.status || 400,
    msg: err.msg || err,
  });
});
app.use(express.static(path.join(__dirname, "client/build")));
app.get("*", function (req, res) {
  res.sendFile(path.resolve(__dirname, "client/build", "index.html"));
});
app.listen(PORT, function () {
  console.log(`Server has started on ${PORT}`);
});
