const UserCTRL = require("./../controllers/user.controller");
const router = require("express").Router();
const uploader = require("./../middlewares/upload");
const emailvalidation = require("./../middlewares/emailvalidation.middleware");
const token_auth = require("./../../../middleware/token_authentication");
const validationpass = require("./../middlewares/validationpass.middleware");
const signupemail = require("./../middlewares/emaillverifyatsignup.middleware");
const emailVerify = require("./../middlewares/emailVerify.middleware");
const emailVerifycode = require("./../middlewares/emailVerifyCode.middleware");
const resetPass = require("./../middlewares/passwordreset.middleware");
const resetPassWithEmail = require("./../middlewares/passwordResetWithEmail.middleware");
const socialemailcheck = require("./../middlewares/socialemailcheck.middleware");
const checkadmin = require("./../middlewares/checkadmin.middleware");

router
  .route("/")
  .get(token_auth, UserCTRL.fetch)
  .post(signupemail, UserCTRL.add);
router.route("/search").get(token_auth, UserCTRL.search);
router.route("/publicuser").get(UserCTRL.publicFetch);
router
  .route("/profilepic")
  .put(token_auth, uploader.single("img"), UserCTRL.updatepic);
router.route("/sendemailverify").get(token_auth, emailVerify, UserCTRL.update);
router.route("/setverify").put(token_auth, emailVerifycode, UserCTRL.update);
router
  .route("/checkadmin")
  .get(checkadmin)
  .post(signupemail, UserCTRL.createadmin);

router
  .route("/id")
  .put(token_auth, emailvalidation, validationpass, UserCTRL.update)
  .get(token_auth, UserCTRL.fetchById)
  .delete(UserCTRL.remove);
router.route("/socialemaicheck").post(socialemailcheck);
router.route("/reset-password").put(resetPass.resetPassword, UserCTRL.update);
router
  .route("/reset-pass-with-email/:token")
  .put(resetPassWithEmail.passWithEmail, UserCTRL.update);

router.route("/:id").put(token_auth, UserCTRL.updateByAdmin);

module.exports = router;
