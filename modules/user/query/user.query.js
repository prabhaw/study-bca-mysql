const UserModel = require("./../../../db").users;
const map_user_data = require("./../helpers/map_user");
const bcrypt = require("bcryptjs");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newUser = {};
    var newMappedUser = map_user_data(newUser, data);
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(data.password, salt, function (err, hashPassword) {
        newMappedUser.password = hashPassword;
        UserModel.create(newMappedUser)
          .then(function (data) {
            resolve(data.dataValues);
          })
          .catch(function (err) {
            reject(err);
          });
      });
    });
  });
}

function find(condition) {
  return new Promise(function (resolve, reject) {
    UserModel.findAll({
      where: condition,
      attributes: [
        "id",
        "first_name",
        "last_name",
        "password",
        "email",
        "role",
        "description",
        "verify",
        "profile_image",
        "university",
        "college",
        "contact",
        "active",
      ],
    })
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function findwithtotal(condition, option = {}) {
  const limit = Number(option.pageSize) || 12;
  const page = Number(option.pageNumber) || 1;
  return new Promise(function (resolve, reject) {
    UserModel.findAll({
      where: condition,
      offset: (page - 1) * limit,
      limit: limit,
      order: [["createdAt", "DESC"]],
      attributes: [
        "id",
        "first_name",
        "last_name",
        "password",
        "email",
        "role",
        "description",
        "verify",
        "profile_image",
        "university",
        "college",
        "contact",
        "active",
      ],
    })
      .then(function (data) {
        UserModel.count({ where: condition }).then(function (total) {
          resolve({ data, total });
        });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function update(id, data) {
  return new Promise(function (resolve, reject) {
    UserModel.findByPk(id)
      .then(function (user) {
        if (user) {
          var oldImage = user.profile_image;
          bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(data.password, salt, function (err, hashPassword) {
              var updatedMappedUser = map_user_data({}, data);
              if (hashPassword) {
                updatedMappedUser.password = hashPassword;
              }
              UserModel.update(updatedMappedUser, {
                where: { id: id },
              })
                .then(function (num) {
                  if (num == 1) {
                    UserModel.findByPk(id, {
                      attributes: [
                        "id",
                        "first_name",
                        "last_name",
                        "password",
                        "email",
                        "role",
                        "description",
                        "verify",
                        "profile_image",
                        "university",
                        "college",
                        "contact",
                        "active",
                      ],
                    })
                      .then(function (user) {
                        resolve({ user: user, oldImage });
                      })
                      .catch(function (err) {
                        reject(err);
                      });
                  } else {
                    reject();
                  }
                })
                .catch(function (err) {
                  reject(err);
                });
            });
          });
        } else {
          reject({ msg: "User Not Found", status: 404 });
        }
      })
      .catch(function (err) {
        resolve(err);
      });
  });
}

function updatepic(id, data) {
  return new Promise(function (resolve, reject) {
    UserModel.findByPk(id)
      .then(function (user) {
        if (user) {
          var oldImage = user.profile_image;

          var updatedMappedUser = map_user_data({}, data);

          UserModel.update(updatedMappedUser, {
            where: { id: id },
          })
            .then(function (num) {
              if (num == 1) {
                UserModel.findByPk(id, {
                  attributes: [
                    "id",
                    "first_name",
                    "last_name",
                    "password",
                    "email",
                    "role",
                    "description",
                    "verify",
                    "profile_image",
                    "university",
                    "college",
                    "contact",
                    "active",
                  ],
                })
                  .then(function (user) {
                    resolve({ user: user, oldImage });
                  })
                  .catch(function (err) {
                    reject(err);
                  });
              } else {
                reject();
              }
            })
            .catch(function (err) {
              reject(err);
            });
        } else {
          reject({ msg: "User Not Found", status: 404 });
        }
      })
      .catch(function (err) {
        resolve(err);
      });
  });
}

function userUpdate(id, data) {
  return new Promise(function (resolve, reject) {
    UserModel.findByPk(id)
      .then(function (user) {
        if (user) {
          var updatedMappedUser = map_user_data({}, data);
          UserModel.update(updatedMappedUser, {
            where: { id: id },
          })
            .then(function (num) {
              if (num == 1) {
                UserModel.findByPk(id, {
                  attributes: [
                    "id",
                    "first_name",
                    "last_name",
                    "password",
                    "email",
                    "role",
                    "description",
                    "verify",
                    "profile_image",
                    "university",
                    "college",
                    "contact",
                    "active",
                  ],
                })
                  .then(function (user) {
                    resolve(user);
                  })
                  .catch(function (err) {
                    reject({ msg: "Error While Fetching User." });
                  });
              } else {
                reject({ msg: "Error Wile Updating" });
              }
            })
            .catch(function (err) {
              reject({ msg: "Error While Updating ." });
            });
        } else {
          reject({ msg: "USer Not Found" });
        }
      })
      .catch(function (err) {});
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    UserModel.findByPk(id).then(function (user) {
      if (user) {
        var Image = user.profile_image;
        UserModel.destroy({ where: { id: id } }).then(function (num) {
          if (num == 1) {
            resolve({ Image, msg: "User is Deleted." });
          } else {
            reject();
          }
        });
      }
    });
  });
}

module.exports = {
  insert,
  find,
  update,
  remove,
  updatepic,
  findwithtotal,
  userUpdate,
};
