"use strict";

module.exports = function (sequelize, DataTypes) {
  const Users = sequelize.define("user", {
    id: {
      defaultValue: DataTypes.UUIDV4,
      type: DataTypes.UUID,
      allowNull: false,
      unique: true,
      primaryKey: true,
    },
    first_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      set(val) {
        this.setDataValue("first_name", val.trim());
      },
    },
    last_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      set(val) {
        this.setDataValue("last_name", val.trim());
      },
    },

    password: {
      type: DataTypes.STRING(50),

      set(val) {
        this.setDataValue("password", val.trim());
      },
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      trim: true,
      unique: true,
      set(val) {
        this.setDataValue("email", val.toLowerCase().trim());
      },
    },
    role: {
      defaultValue: 0,
      type: DataTypes.INTEGER(2),
    },
    description: {
      type: DataTypes.TEXT,
    },
    verify: {
      defaultValue: false,
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    verifycode: {
      type: DataTypes.STRING(225),
      defaultValue: null,
    },
    university: {
      type: DataTypes.STRING(225),
    },
    college: {
      type: DataTypes.STRING(225),
    },
    contact: {
      type: DataTypes.STRING(10),
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },
    verifyCodeExpiry: DataTypes.DATE,
    passwordResetToken: DataTypes.STRING(200),
    passwordResetTokenExpiry: DataTypes.DATE,
    profile_image: DataTypes.TEXT,
  });
  return Users;
};
