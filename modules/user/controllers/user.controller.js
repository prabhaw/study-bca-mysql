const UserQuery = require("./../query/user.query");
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
function add(req, res, next) {
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }
  var data = req.body;
  if (req.file) {
    data.profile_image = req.file.filename;
  }

  UserQuery.insert(data)
    .then(function (done) {
      res.status(200).json(done);
    })
    .catch(function (err) {
      console.log(err);
      next({ msg: "Error While SignUp" });
    });
}

function createadmin(req, res, next) {
  var data = req.body;
  data.role = 1;
  UserQuery.insert(data)
    .then(function () {
      res.status(200).json({ msg: "User Created Succes." });
    })
    .catch(function (err) {
      console.log(err);
      next({ msg: "Error While SignUp" });
    });
}

function fetch(req, res, next) {
  var condition = {};

  if (req.loggedInUser.role != 1) {
    condition.id = { id: req.loggedInUser.id };
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;
  UserQuery.findwithtotal(condition, pageOptions)
    .then(function (done) {
      res.status(200).json(done);
    })
    .catch(function (err) {
      console.log(err);
      next({ msg: "Error While Fetching User." });
    });
}

// ----------------------------------------------------------
function publicFetch(req, res, next) {
  var condition = { role: 2, active: true };
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;
  UserQuery.findwithtotal(condition, pageOptions)
    .then(function (done) {
      res.status(200).json(done);
    })
    .catch(function (err) {
      console.log(err);
      next({ msg: "Error While Fetching User." });
    });
}
// ----------------------------------------------------------

function fetchById(req, res, next) {
  var condition = { id: req.loggedInUser.id };
  UserQuery.find(condition)
    .then(function (data) {
      res.status(200).json(data[0]);
    })
    .catch(function (err) {
      next({ msg: "Cant't Get the User." });
    });
}

function update(req, res, next) {
  var id = req.loggedInUser.id;
  const data = req.body;

  UserQuery.update(id, data)
    .then(function (users) {
      res.status(200).json(users.user);
      if (req.file && users.oldImage) {
        fs.unlink(
          path.join(process.cwd(), "uploads/user_image/", users.oldImage),
          function (err, done) {
            if (err) {
              console.log("Error While Removing File");
            } else {
              console.log("Remove Successfully");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next({ msg: "Error While Updationg User" });
    });
}

function updatepic(req, res, next) {
  var id = req.loggedInUser.id;
  const data = req.body;
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }
  if (req.file) {
    data.profile_image = req.file.filename;
    console.log(data);
  }

  UserQuery.updatepic(id, data)
    .then(function (users) {
      res.status(200).json(users.user);
      if (req.file && users.oldImage) {
        fs.unlink(
          path.join(process.cwd(), "uploads/user_image/", users.oldImage),
          function (err, done) {
            if (err) {
              console.log("Error While Removing File");
            } else {
              console.log("Remove Successfully");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next({ msg: "Error While Updationg User" });
    });
}

function updateByAdmin(req, res, next) {
  if (req.loggedInUser.role == 1) {
    const data = req.body;
    const id = req.params.id;
    UserQuery.userUpdate(id, data)
      .then(function (users) {
        res.status(200).json(users);
      })
      .catch(function (err) {
        next(err);
      });
  } else {
    next({ msg: "You are not Administrator." });
  }
}

function remove(req, res, next) {
  UserQuery.remove(req.params.id).then(function (data) {
    res.status(200).json(data.msg);
    if (data.Image) {
      fs.unlink(
        path.join(process.cwd(), "uploads/user_image/", data.Image),
        function (err, done) {
          if (err) {
            console.log("Error While Removing File");
          } else {
            console.log("Remove Successfully");
          }
        }
      );
    }
  });
}

function search(req, res, next) {
  const search_value = req.query.search;
  var condition = {};
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  if (req.loggedInUser.role == 1) {
    var mapSearch = {};
    if (search_value) {
      mapSearch = {
        [Op.and]: [
          condition,
          {
            [Op.or]: [
              { first_name: { [Op.like]: "%" + search_value + "%" } },
              { last_name: { [Op.like]: "%" + search_value } },
              { email: search_value },
            ],
          },
        ],
      };
    } else {
      mapSearch = condition;
    }
    UserQuery.findwithtotal(mapSearch, pageOptions)
      .then(function (data) {
        res.status(200).json(data);
      })
      .catch(function (err) {
        console.log(err);
        next({ msg: "ERROR While Searching Data" });
      });
  } else {
    next({ msg: "You are not Administrator." });
  }
}

module.exports = {
  add,
  fetch,
  update,
  fetchById,
  remove,
  search,
  updatepic,
  createadmin,
  updateByAdmin,
  publicFetch,
};
