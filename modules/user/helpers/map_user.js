module.exports = function (user, userDetails) {
  if (userDetails.first_name) {
    user.first_name = userDetails.first_name;
  }
  if (userDetails.last_name) {
    user.last_name = userDetails.last_name;
  }

  if (userDetails.password) {
    user.password = userDetails.password;
  }
  if (userDetails.email) {
    user.email = userDetails.email;
  }
  if (userDetails.role) {
    user.role = userDetails.role;
  }
  if (userDetails.description || userDetails.description == "") {
    user.description = userDetails.description;
  }
  if (userDetails.verify || userDetails.verify == false) {
    user.verify = userDetails.verify;
  }
  if (userDetails.verifycode || userDetails.verifycode == null) {
    user.verifycode = userDetails.verifycode;
  }
  if (userDetails.verifyCodeExpiry) {
    user.verifyCodeExpiry = userDetails.verifyCodeExpiry;
  }
  if (userDetails.passwordResetToken) {
    user.passwordResetToken = userDetails.passwordResetToken;
  }
  if (userDetails.passwordResetTokenExpiry) {
    user.passwordResetTokenExpiry = userDetails.passwordResetTokenExpiry;
  }
  if (userDetails.profile_image) {
    user.profile_image = userDetails.profile_image;
  }
  if (userDetails.provider) {
    user.provider = userDetails.provider;
  }
  if (userDetails.providerid) {
    user.providerid = userDetails.providerid;
  }
  if (userDetails.university || userDetails.university == "") {
    user.university = userDetails.university;
  }
  if (userDetails.college || userDetails.college == "") {
    user.college = userDetails.college;
  }
  if (userDetails.contact || userDetails.contact == "") {
    user.contact = userDetails.contact;
  }

  if (userDetails.active || userDetails.active == false) {
    user.active = userDetails.active;
  }
  return user;
};
