const UserModel = require("./../../../db").users;

function socialemailcheck(req, res, next) {
  UserModel.findAll({
    where: {
      role: 1,
    },
  })
    .then(function (user) {

      if (user.length) {
        next({ msg: "Application Already Have an Administrator." });
      } else {
        res.status(200).json({ msg: "There is no accoutn" });
      }
    })
    .catch(function (err) {
      console.log(err);
      next({ msg: "Error While getting User" });
    });
}

module.exports = socialemailcheck;
