const UserModel = require("./../../../db").users;
const randomstring = require("randomstring");
const sender = require("./../../../config/nodemailer.config");
const mailVerifyTemplate = require("./../../../templates/email_template");

function emailVerify(req, res, next) {
  var id = req.loggedInUser.id;
  UserModel.findByPk(id)
    .then(function (user) {
      const verifycode = randomstring.generate(10);
      const mailData = mailVerifyTemplate.auth.varifyEmail(user, verifycode);
      sender.sendMail(mailData, function (err, done) {
        if (err) {
          return next({ msg: "Error While Sending Email !" });
        }
        if (done) {
          req.body.verifyCodeExpiry = Date.now() + 30000;
          req.body.verifycode = verifycode;
          next();
        }
      });
    })
    .catch(function (err) {
      next({ msg: "Error While Getting User !" });
    });
}

module.exports = emailVerify;
