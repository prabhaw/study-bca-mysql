const UserModel = require("./../../../db").users;

function emailvalidation(req, res, next) {
  if (req.body.email) {
    UserModel.findOne({
      where: {
        email: req.body.email,
      },
    })
      .then(function (user) {
        if (user) {
          UserModel.findByPk(req.loggedInUser.id)
            .then(function (data) {
              if (data.email == req.body.email) {
                req.body.email = null;
                next();
              } else {
                next({ msg: "Email is Already Taken." });
              }
            })
            .catch(function (err) {
              next({ msg: "Error While Checking Email" });
            });
        } else {
          req.body.verify = false;
          next();
        }
      })
      .catch(function (err) {
        next({ msg: "Error While getting Email!" });
      });
  } else {
    next();
  }
}

module.exports = emailvalidation;
