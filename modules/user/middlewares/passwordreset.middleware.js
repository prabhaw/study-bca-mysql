const UserModel = require("./../../../db").users;
const sender = require("./../../../config/nodemailer.config");
const mailVerifyTemplate = require("./../../../templates/email_template");
const jwt = require("jsonwebtoken");
const JWT_secret = process.env.JWT_Secret_pass_reset;

function createToken(data) {
  var token = jwt.sign(
    {
      id: data.id,
    },
    JWT_secret
  );
  return token;
}

function resetPassword(req, res, next) {
  var email = req.body.email;
  console.log("email>>", email);
  UserModel.findOne({
    where: {
      email: email,
    },
  })
    .then(function (user) {
      if (user) {
        req.loggedInUser = user;
        const token = createToken(user);
        const verifylink = req.headers.origin + "/reset-password/" + token;
        const mailData = mailVerifyTemplate.auth.reset_password(
          user,
          verifylink
        );

        sender.sendMail(mailData, function (err, done) {
          if (err) {
            return next({ msg: "Error While Sending Email." });
          }
          if (done) {
            req.body.verifyCodeExpiry = Date.now() + 30000;
            req.body.verifycode = token;
            next();
          }
        });
      } else {
        next({ msg: "Your Email is not Register.", status: 404 });
      }
    })
    .catch(function (err) {
      next({ msg: "Error While Getting User." });
    });
}

module.exports = {
  resetPassword,
};
