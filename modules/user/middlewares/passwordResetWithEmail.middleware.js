const UserModel = require("./../../../db").users;
const jwt = require("jsonwebtoken");
const config = process.env.JWT_Secret_pass_reset;

function passWithEmail(req, res, next) {
  var token;

  if (req.params.token) {
    token = req.params.token;
  }

  if (token) {
    jwt.verify(token, config, function (err, decoded) {
      if (err) {
        next({ msg: "Error in Token Checking" });
      }

      UserModel.findByPk(decoded.id)
        .then(function (user) {
          if (user) {
            if (user.verifyCodeExpiry > Date.now()) {
              if (token === user.verifycode) {
                req.loggedInUser = user;
                req.body.password = req.body.newpassword;
                req.body.verifycode = null;
                return next();
              } else {
                return next({ msg: "Verification Link Doesn't Match." });
              }
            } else {
              return next({
                msg: "Verification Link Expired.",
              });
            }
          }
        })
        .catch(function (err) {
          if (err) {
            return next({ msg: "Verification Link Doesn't Match" });
          }
        });
    });
  } else {
    next({
      msg: "Link Doesn't Match.",
    });
  }
}

module.exports = {
  passWithEmail,
};
