const UserModel = require("./../../../db").users;

function emailVerifyCode(req, res, next) {
  var id = req.loggedInUser.id;
  UserModel.findByPk(id)
    .then(function (user) {
      if (user) {
        if (user.verifyCodeExpiry > Date.now()) {
          if (req.body.verifycode === user.verifycode) {
            req.body.verifycode = null;
            req.body.verify = true;
            next();
          } else {
            next({ msg: "Verification Code Doesn't Match!" });
          }
        } else {
          next({ msg: "Verification Code Expired!" });
        }
      }
    })
    .catch(function (err) {
      next({ msg: "Error While Verifying Account." });
    });
}

module.exports = emailVerifyCode;
