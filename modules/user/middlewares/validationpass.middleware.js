const UserModel = require("./../../../db").users;
var bcrypt = require("bcryptjs");

function validationPass(req, res, next) {
  var condition = {
    id: req.loggedInUser.id,
  };
  UserModel.findOne({
    where: condition,
  })
    .then(function (user) {
      if (user) {
        bcrypt.compare(req.body.password, user.password, function (
          err,
          isMatch
        ) {
          if (isMatch) {
            if (req.body.newpassword) {
              req.body.password = req.body.newpassword;
            }
            next();
          } else {
            next({ msg: "Invalid User Password" });
          }
        });
      } else {
        next({ msg: "Invalid User" });
      }
    })
    .catch(function (err) {
      next({ msg: "Errro While Getting User." });
    });
}

module.exports = validationPass;
