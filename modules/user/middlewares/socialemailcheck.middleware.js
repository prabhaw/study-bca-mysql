const UserModel = require("./../../../db").users;

function socialemailcheck(req, res, next) {
  if (req.body.email) {
    UserModel.findOne({
      where: {
        email: req.body.email,
      },
    })
      .then(function (user) {
        if (user) {
          next({ msg: "Accoutn is already Register with this Email." });
        } else {
          res.status(200).json({ msg: "Email is not taken" });
        }
      })
      .catch(function (err) {
        console.log(err);
        next({ msg: "Error While getting Email!" });
      });
  } else {
    next({ msg: "Email is Required To Create an Account." });
  }
}

module.exports = socialemailcheck;
