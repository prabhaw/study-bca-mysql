const UserModel = require("./../../../db").users;

function signupemailvalidation(req, res, next) {
  if (req.body.email) {
    UserModel.findOne({
      where: {
        email: req.body.email,
      },
    })
      .then(function (user) {
        if (user) {
          next({ msg: "Email is Already Taken." });
        } else {
          next();
        }
      })
      .catch(function (err) {
        next({ msg: "Error While getting Email!" });
      });
  } else {
    next();
  }
}

module.exports = signupemailvalidation;
