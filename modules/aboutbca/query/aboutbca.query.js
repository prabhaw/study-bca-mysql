const db = require("./../../../db");
const map_aboutbca_data = require("./../helper/map_aboutbca");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newAboutBCA = {};
    var newMappedAboutbac = map_aboutbca_data(newAboutBCA, data);
    db.aboutbca
      .create(newMappedAboutbac)
      .then(function (data) {
        db.aboutbca
          .findOne({
            where: { id: data.id },
            include: [
              {
                model: db.users,
                attributes: [
                  "first_name",
                  "last_name",
                  "role",
                  "profile_image",
                  "verify",
                  "active",
                ],
              },
            ],
          })
          .then(function (data) {
            resolve(data);
          })
          .catch(function (err) {
            reject(err);
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function find(condition, subcondition = {}, option = {}) {
  return new Promise(function (resolve, reject) {
    var limit = Number(option.pageSize) || 12;
    var page = Number(option.pageNumber) || 1;

    db.aboutbca
      .findAll({
        where: condition,
        offset: (page - 1) * limit,
        limit: limit,
        order: [["created_at", "DESC"]],
        include: [
          {
            model: db.users,
            where: subcondition,
            attributes: [
              "first_name",
              "last_name",
              "role",
              "profile_image",
              "verify",
              "active",
              "email",
            ],
          },
        ],
      })
      .then(function (data) {
        db.aboutbca
          .count({
            where: condition,

            include: [{ model: db.users, where: subcondition }],
          })
          .then(function (total) {
            resolve({ data, total });
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function findOne(condition, subcondition = {}) {
  return db.aboutbca.findOne({
    where: condition,
    include: [
      {
        model: db.users,
        where: subcondition,
        attributes: [
          "first_name",
          "last_name",
          "role",
          "verify",
          "active",
          "profile_image",
        ],
      },
    ],
  });
}

function update(id, data) {
  return new Promise(function (resolve, reject) {
    db.aboutbca
      .findByPk(id)
      .then(function (aboutbca) {
        if (aboutbca) {
          var updatedMapped = map_aboutbca_data({}, data);
          db.aboutbca
            .update(updatedMapped, { where: { id: id } })
            .then(function (num) {
              if (num == 1) {
                db.aboutbca
                  .findOne({
                    where: { id: aboutbca.id },
                    include: [
                      {
                        model: db.users,
                        attributes: [
                          "first_name",
                          "last_name",
                          "email",
                          "verify",
                          "active",
                          "profile_image",
                        ],
                      },
                    ],
                  })
                  .then(function (data) {
                    resolve(data);
                  })
                  .catch(function (err) {
                    reject(err);
                  });
              } else {
                reject({ msg: "You have not write about BCA." });
              }
            });
        }
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.aboutbca
      .findByPk(id)
      .then(function (aboutbca) {
        if (aboutbca) {
          db.aboutbca.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve();
            } else {
              reject({ msg: "Can't Delete Post." });
            }
          });
        } else {
          reject({ msg: "Post Not Found." });
        }
      })
      .catch(function (err) {
        reject({ msg: "Post NOT Found" });
      });
  });
}

module.exports = { insert, find, update, remove, findOne };
