
const router = require("express").Router();
const BcaCTRL = require("./../controllers/aboutbca.controller");
const token_auth = require("./../../../middleware/token_authentication");

router
  .route("/")
  .get(token_auth, BcaCTRL.fetchByuser)
  .post(token_auth, BcaCTRL.add);
router.route("/admin").get(token_auth, BcaCTRL.fetchAdmin);
router.route("/public").get(BcaCTRL.publicfetch);
router.route("/search").get(token_auth, BcaCTRL.searchAdmin);
router
  .route("/:id")
  .put(token_auth, BcaCTRL.update)
  .delete(token_auth, BcaCTRL.remove);

module.exports = router;
