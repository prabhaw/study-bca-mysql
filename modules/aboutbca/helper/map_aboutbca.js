module.exports = function (bca, bcaDetails) {
  if (bcaDetails.user_id) {
    bca.user_id = bcaDetails.user_id;
  }
  if (bcaDetails.short_description) {
    bca.short_description = bcaDetails.short_description;
  }
  if (bcaDetails.description) {
    bca.description = bcaDetails.description;
  }
  if (bcaDetails.verified == false || bcaDetails.verified) {
    bca.verified = bcaDetails.verified;
  }
  return bca;
};
