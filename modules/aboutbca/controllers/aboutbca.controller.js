const BcaQuery = require("./../query/aboutbca.query");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

function add(req, res, next) {
  const data = req.body;
  data.user_id = req.loggedInUser.id;

  BcaQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error Occour" });
    });
}

function fetchAdmin(req, res, next) {
  var condition = {};
  if (req.loggedInUser.role == 1) {
    var pageOption = {};
    pageOption.pageSize = req.query.pageSize;
    pageOption.pageNumber = req.query.pageNumber;
    BcaQuery.find(condition, {}, pageOption)
      .then(function (data) {
        res.status(200).json(data);
      })
      .catch(function (err) {
        next(err.msg || { msg: "Error While Fetching Post." });
      });
  } else {
    next({ msg: "You are Not Admin" });
  }
}

function publicfetch(req, res, next) {
  var condition = { verified: true };

  var pageOption = {};
  pageOption.pageSize = req.query.pageSize;
  pageOption.pageNumber = req.query.pageNumber;
  BcaQuery.find(condition, { active: true, verify: true }, pageOption)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Fetching Post." });
    });
}

function fetchByuser(req, res, next) {
  var condition = { user_id: req.loggedInUser.id };

  BcaQuery.findOne(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Geting Post." });
    });
}

function update(req, res, next) {
  var id = req.params.id;
  const data = req.body;
  BcaQuery.update(id, data)
    .then(function (done) {
      res.status(200).json(done);
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Updating Post." });
    });
}
function remove(req, res, next) {
  BcaQuery.remove(req.params.id)
    .then(function (data) {
      res.status(200).json({ msg: "Post Successfully Deleted" });
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Removing Post." });
    });
}

function searchAdmin(req, res, next) {
  const search_value = req.query.search;
  const condition = {};
  var pageOption = {};
  pageOption.pageSize = req.query.pageSize;
  pageOption.pageNumber = req.query.pageNumber;

  if (req.loggedInUser.role == 1) {
    var mapSearch = {};
    if (search_value) {
      mapSearch = {
        [Op.or]: [
          {
            first_name: { [Op.like]: "%" + search_value + "%" },
          },
          { email: search_value },
        ],
      };
    }
    BcaQuery.find(condition, mapSearch, pageOption)
      .then(function (data) {
        res.status(200).json(data);
      })
      .catch(function (err) {
        next(err.msg || { msg: "Error While Fetching Post." });
      });
  } else {
    next({ msg: "You are Not Admin" });
  }
}

module.exports = {
  add,
  fetchAdmin,
  publicfetch,
  fetchByuser,
  update,
  remove,
  searchAdmin,
};
