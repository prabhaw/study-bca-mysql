const multer = require("multer");

var Storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
  destination: function (req, file, cb) {
    cb(null, "./uploads/books");
  },
});

function filter(req, file, cb) {
  var mimeType = file.mimetype.split("/")[0];
  if (mimeType !== "image") {
    req.fileErr = true;
    cb(null, false);
  } else {
    cb(null, true);
  }
}

var uploads = multer({
  storage: Storage,
  fileFilter: filter,
});

module.exports = uploads;
