"use strict";

module.exports = function (sequelize, DataTypes) {
  const Books = sequelize.define(
    "book",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },
      university: {
        type: DataTypes.STRING(225),
        allowNull: false,
      },
      semester: { type: DataTypes.STRING(225), allowNull: false },
      subject: { type: DataTypes.STRING(225), allowNull: false },
      name: { type: DataTypes.STRING(225), allowNull: false },
      author: { type: DataTypes.STRING(225), allowNull: false },
      publication: { type: DataTypes.STRING(225), allowNull: false },
      book_img: { type: DataTypes.STRING(225), allowNull: false },
    },
    {
      underscored: true,
    }
  );
  return Books;
};
