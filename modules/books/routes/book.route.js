const router = require("express").Router();
const BookCTRL = require("./../controllers/book.controller");
const token_auth = require("./../../../middleware/token_authentication");
const uploader = require("./../middlewares/upload");

router
  .route("/")
  .get(BookCTRL.fetch)
  .post(token_auth, uploader.single("book_img"), BookCTRL.add);
router.route("/search").get(BookCTRL.search);
router.route("/:id").delete(token_auth, BookCTRL.remove);

module.exports = router;
