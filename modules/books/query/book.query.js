const db = require("./../../../db");
const map_book_data = require("./../helper/map_book");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newBook = {};
    var newMappedBook = map_book_data(newBook, data);
    db.books
      .create(newMappedBook)
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        resolve(err);
      });
  });
}

function find(condition, option = {}) {
  return new Promise(function (resolve, reject) {
    var limit = Number(option.pageSize) || 5;
    var page = Number(option.pageNumber) || 1;

    db.books
      .findAll({
        where: condition,
        offset: (page - 1) * limit,
        limit: limit,
        order: [["semester", "ASC"]],
      })
      .then(function (data) {
        db.books
          .count({
            where: condition,
          })
          .then(function (total) {
            resolve({ data, total });
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.books
      .findByPk(id)
      .then(function (book) {
        if (book) {
          var Image = book.book_img;
          db.books.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve({ Image });
            } else {
              reject();
            }
          });
        } else {
          reject({ msg: "Book Not Found." });
        }
      })
      .catch(function (err) {
        reject({ msg: "Book NOt FOund." });
      });
  });
}

module.exports = { insert, find, remove };
