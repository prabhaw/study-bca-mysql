module.exports = function (book, bookDetails) {
  if (bookDetails.university) {
    book.university = bookDetails.university;
  }
  if (bookDetails.semester) {
    book.semester = bookDetails.semester;
  }
  if (bookDetails.subject) {
    book.subject = bookDetails.subject;
  }
  if (bookDetails.name) {
    book.name = bookDetails.name;
  }
  if (bookDetails.author) {
    book.author = bookDetails.author;
  }
  if (bookDetails.publication) {
    book.publication = bookDetails.publication;
  }
  if (bookDetails.book_img) {
    book.book_img = bookDetails.book_img;
  }
  return book;
};
