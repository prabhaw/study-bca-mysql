const BookQuery = require("./../query/book.query");
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const { EDEADLK } = require("constants");
const Op = Sequelize.Op;

function add(req, res, next) {
  if (req.fileErr) {
    return next({ msg: "Invalid Filre Format", status: 400 });
  }
  var data = req.body;
  if (req.file) {
    data.book_img = req.file.filename;
  }
  BookQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error On Adding Book." });
    });
}

function fetch(req, res, next) {
  var condition = {};
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  BookQuery.find(condition, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Books." });
    });
}

function remove(req, res, next) {
  BookQuery.remove(req.params.id)
    .then(function (data) {
      res.status(200).json({ msg: "Successfully Deleted." });
      if (data.Image) {
        fs.unlink(
          path.join(process.cwd(), "uploads/books/" + data.Image),
          function (err, done) {
            if (err) {
              console.log("Error While Removing Image.");
            } else {
              console.log("Remove Book Successfully");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Removing Blog." });
    });
}

function search(req, res, next) {
  const condition = {};
  const university = req.query.university;
  const semester = req.query.semester;
  const subject = req.query.subject;
  if (university) {
    condition.university = university;
  }
  if (semester) {
    condition.semester = semester;
  }
  if (subject) {
    condition.subject = subject;
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  BookQuery.find(condition, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Searching Syllabus." });
    });
}

module.exports = { add, fetch, remove, search };
