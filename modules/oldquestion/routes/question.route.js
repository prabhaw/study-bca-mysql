const router = require("express").Router();
const QusCTRL = require("./../controllers/question.controller");
const token_auth = require("./../../../middleware/token_authentication");
const uploader = require("./../middlewares/upload");

router
  .route("/")
  .get(QusCTRL.publicfetch)
  .post(token_auth, uploader.single("old_question"), QusCTRL.add);

router.route("/userfetch").get(token_auth, QusCTRL.fetchdash);
router.route("/search").get(QusCTRL.search);

router
  .route("/:id")
  .get(QusCTRL.fetchSingle)
  .delete(token_auth, QusCTRL.remove);

module.exports = router;
