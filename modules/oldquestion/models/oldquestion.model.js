"use strict";
const moment = require("moment");

module.exports = function (sequelize, DataTypes) {
  const OldQuestion = sequelize.define(
    "oldquestion",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },
      user_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      subject: {
        type: DataTypes.STRING(225),
      },
      university: {
        type: DataTypes.STRING(225),
      },
      semester: {
        type: DataTypes.STRING(225),
      },
      year: {
        type: DataTypes.INTEGER(4),
      },
      exam_time: {
        type: DataTypes.STRING(50),
      },
      old_question: {
        type: DataTypes.TEXT,
      },
      publish_date: {
        defaultValue: moment().format("MMM Do YYYY"),
        type: DataTypes.STRING(20),
      },
    },
    {
      underscored: true,
    }
  );
  return OldQuestion;
};
