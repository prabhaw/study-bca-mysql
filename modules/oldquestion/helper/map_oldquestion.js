module.exports = function (question, questionDetails) {
  if (questionDetails.user_id) {
    question.user_id = questionDetails.user_id;
  }
  if (questionDetails.subject) {
    question.subject = questionDetails.subject;
  }
  if (questionDetails.university) {
    question.university = questionDetails.university;
  }
  if (questionDetails.semester) {
    question.semester = questionDetails.semester;
  }

  if (questionDetails.year) {
    question.year = questionDetails.year;
  }
  if (questionDetails.exam_time) {
    question.exam_time = questionDetails.exam_time;
  }
  if (questionDetails.old_question) {
    question.old_question = questionDetails.old_question;
  }

  return question;
};
