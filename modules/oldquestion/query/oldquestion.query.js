const db = require("./../../../db");
const map_question_data = require("./../helper/map_oldquestion");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newQuestion = {};
    var newMappedQuestion = map_question_data(newQuestion, data);
    db.question
      .create(newMappedQuestion)
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function find(condition, subcondition = {}, option = {}) {
  return new Promise(function (resolve, reject) {
    var limit = Number(option.pageSize) || 12;
    var page = Number(option.pageNumber) || 1;

    db.question
      .findAll({
        where: condition,
        offset: (page - 1) * limit,
        limit: limit,
        order: [["created_at", "DESC"]],
        include: [
          {
            model: db.users,
            where: subcondition,
            attributes: ["first_name", "last_name"],
          },
        ],
      })
      .then(function (data) {
        db.question
          .count({
            where: condition,
            include: [{ model: db.users, where: subcondition }],
          })
          .then(function (total) {
            resolve({ data, total });
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function findOne(condition, subcondition = {}) {
  return db.question.findOne({
    where: condition,
    include: [
      {
        model: db.users,
        where: subcondition,
        attributes: [
          "first_name",
          "last_name",
          "email",
          "verify",
          "profile_image",
        ],
      },
    ],
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.question
      .findByPk(id)
      .then(function (question) {
        if (question) {
          var File = question.old_question;
          db.question.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve({ File });
            } else {
              reject();
            }
          });
        } else {
          reject({ msg: "Question Not FOund." });
        }
      })
      .catch(function (err) {
        reject({ msg: "Question Was not Found." });
      });
  });
}

module.exports = { insert, find, remove, findOne };
