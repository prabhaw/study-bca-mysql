const multer = require("multer");

var Storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
  destination: function (req, file, cb) {
    cb(null, "./uploads/question");
  },
});

function filter(req, file, cb) {
  if (file.mimetype !== "application/pdf") {
    req.fileErr = true;
    cb(null, false);
  } else {
    cb(null, true);
  }
}

var uploads = multer({
  storage: Storage,
  fileFilter: filter,
});

module.exports = uploads;
