const QusQuery = require("./../query/oldquestion.query");
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

function add(req, res, next) {
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }
  var data = req.body;
  data.user_id = req.loggedInUser.id;

  if (req.file) {
    data.old_question = req.file.filename;
  }

  QusQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Notes." });
    });
}

function fetchdash(req, res, next) {
  var condition = {};
  if (req.loggedInUser.role != 1) {
    condition.user_id = req.loggedInUser.id;
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;
  QusQuery.find(condition, {}, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Qldquestion." });
    });
}

function remove(req, res, next) {
  QusQuery.remove(req.params.id)
    .then(function (data) {
      res.status(200).json({ msg: "Successfully  Deleted. " });
      if (data.File) {
        fs.unlink(
          path.join(process.cwd(), "uploads/question/" + data.File),
          function (err, done) {
            if (err) {
              console.log("Error while removeing File.");
            } else {
              console.log("Remove Successfully.");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Removing Question." });
    });
}

function publicfetch(req, res, next) {
  var condition = {};
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  QusQuery.find(condition, { active: true, verify: true }, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fechign Question" });
    });
}

function fetchSingle(req, res, next) {
  var condition = {
    id: req.params.id,
  };
  QusQuery.findOne(condition, { active: true, verify: true })
    .then(function (data) {
      if (data) {
        res.status(200).json(data);
      } else {
        next({ msg: "Post Not Found." });
      }
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Question." });
    });
}

function search(req, res, next) {
  const condition = {};
  const university = req.query.university;
  const semester = req.query.semester;
  const subject = req.query.subject;
  if (university) {
    condition.university = university;
  }
  if (semester) {
    condition.semester = semester;
  }
  if (subject) {
    condition.subject = subject;
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  QusQuery.find(condition, { active: true, verify: true }, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fechign Question" });
    });
}

module.exports = {
  add,
  fetchdash,
  remove,
  publicfetch,
  fetchSingle,
  search,
};
