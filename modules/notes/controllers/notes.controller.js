const NoteQuery = require("./../query/notes.query");
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

function add(req, res, next) {
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }
  var data = req.body;
  data.user_id = req.loggedInUser.id;
  if (req.file) {
    data.note = req.file.filename;
  }
  NoteQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error on Adding Note." });
    });
}

function fetchdash(req, res, next) {
  var condition = {};
  if (req.loggedInUser.role != 1) {
    condition.user_id = req.loggedInUser.id;
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  NoteQuery.find(condition, {}, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Notes." });
    });
}

function update(req, res, next) {
  var id = req.params.id;
  const data = req.body;
  if (req.fileErr) {
    return next({ msg: "Invalid File Format", status: 400 });
  }
  if (req.file) {
    data.note = req.file.filename;
  }

  NoteQuery.update(id, data)
    .then(function (note) {
      res.status(200).json(note.data);
      if (req.file && note.OldFile) {
        fs.unlink(
          path.join(process.cwd(), "uploads/notes/" + note.OldFile),
          function (err, done) {
            if (err) {
              console.log("Error When Removing File");
            } else {
              console.log("Remove Successfully.");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Updating Note." });
    });
}

function remove(req, res, next) {
  NoteQuery.remove(req.params.id)
    .then(function (data) {
      res.status(200).json({ msg: "Successfully  Deleted. " });
      if (data.File) {
        fs.unlink(
          path.join(process.cwd(), "uploads/notes/" + data.File),
          function (err, done) {
            if (err) {
              console.log("Error while removeing File.");
            } else {
              console.log("Remove Successfully.");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Removing Note." });
    });
}

function publicfetch(req, res, next) {
  var condition = { verified: true };
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  NoteQuery.find(condition, { active: true, verify: true }, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fechign Notes" });
    });
}

function fetchSingle(req, res, next) {
  var condition = {
    [Op.and]: [{ id: req.params.id }, { verified: true }],
  };
  NoteQuery.findOne(condition, { active: true, verify: true })
    .then(function (data) {
      if (data) {
        res.status(200).json(data);
      } else {
        next({ msg: "Post Not Found." });
      }
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Note." });
    });
}

function search(req, res, next) {
  const condition = { verified: true };
  const university = req.query.university;
  const semester = req.query.semester;
  const subject = req.query.subject;
  if (university) {
    condition.university = university;
  }
  if (semester) {
    condition.semester = semester;
  }
  if (subject) {
    condition.subject = subject;
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  NoteQuery.find(condition, { active: true, verify: true }, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fechign Notes" });
    });
}

module.exports = {
  add,
  fetchdash,
  update,
  remove,
  publicfetch,
  fetchSingle,
  search,
};
