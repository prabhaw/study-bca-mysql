const db = require("./../../../db");
const map_note_data = require("./../helper/map_notes");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newNote = {};
    var newMappedNote = map_note_data(newNote, data);
    db.notes
      .create(newMappedNote)
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function find(condition, subcondition = {}, option = {}) {
  return new Promise(function (resolve, reject) {
    var limit = Number(option.pageSize) || 12;
    var page = Number(option.pageNumber) || 1;

    db.notes
      .findAll({
        where: condition,
        offset: (page - 1) * limit,
        limit: limit,
        order: [["created_at", "DESC"]],
        include: [
          {
            model: db.users,
            where: subcondition,
            attributes: ["first_name", "last_name", "verify"],
          },
        ],
      })
      .then(function (data) {
        db.notes
          .count({
            where: condition,
            include: [
              {
                model: db.users,
                where: subcondition,
              },
            ],
          })
          .then(function (total) {
            resolve({ data, total });
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function findOne(condition, subcondition = {}) {
  return db.notes.findOne({
    where: condition,
    include: [
      {
        model: db.users,
        where: subcondition,
        attributes: [
          "first_name",
          "last_name",
          "email",
          "verify",
          "profile_image",
        ],
      },
    ],
  });
}

function update(id, data) {
  return new Promise(function (resolve, reject) {
    db.notes.findByPk(id).then(function (note) {
      if (note) {
        var OldFile = note.note;
        var updatedMappedNote = map_note_data({}, data);
        db.notes
          .update(updatedMappedNote, { where: { id: id } })
          .then(function (num) {
            if (num == 1) {
              db.notes
                .findOne({
                  where: { id: note.id },
                })
                .then(function (data) {
                  resolve({ data, OldFile });
                })
                .catch(function (err) {
                  reject(err);
                });
            }
          });
      } else {
        reject({ msg: "Blog Not Found", status });
      }
    });
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.notes
      .findByPk(id)
      .then(function (note) {
        if (note) {
          var File = note.note;
          db.notes.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve({ File });
            } else {
              reject();
            }
          });
        } else {
          reject({ msg: "Note Not Found." });
        }
      })
      .catch(function (err) {
        reject({ msg: "Note Was Not Found" });
      });
  });
}

module.exports = { insert, find, update, remove, findOne };
