module.exports = function (note, noteDetails) {
  if (noteDetails.user_id) {
    note.user_id = noteDetails.user_id;
  }

  if (noteDetails.subject) {
    note.subject = noteDetails.subject;
  }

  if (noteDetails.university) {
    note.university = noteDetails.university;
  }

  if (noteDetails.semester) {
    note.semester = noteDetails.semester;
  }
  if (noteDetails.note) {
    note.note = noteDetails.note;
  }
  if (noteDetails.verified || noteDetails.verified == false) {
    note.verified = noteDetails.verified;
  }
  if (noteDetails.description || noteDetails.description == "") {
    note.description = noteDetails.description;
  }
  return note;
};
