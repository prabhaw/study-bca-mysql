"use strict";

const moment = require("moment");

module.exports = function (sequelize, DataTypes) {
  const Note = sequelize.define(
    "note",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },
      user_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      subject: {
        type: DataTypes.STRING(225),
      },
      university: {
        type: DataTypes.STRING(225),
      },
      semester: {
        type: DataTypes.STRING(225),
      },
      note: {
        type: DataTypes.TEXT,
      },
      publish_date: {
        defaultValue: moment().format("MMM Do YYYY"),
        type: DataTypes.STRING(20),
      },
      verified: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      description: {
        type: DataTypes.TEXT,
      },
    },
    {
      underscored: true,
    }
  );
  return Note;
};
