const router = require("express").Router();
const NoteCTRL = require("./../controllers/notes.controller");
const token_auth = require("./../../../middleware/token_authentication");
const uploader = require("./../middlewares/upload");

router
  .route("/")
  .get(NoteCTRL.publicfetch)
  .post(token_auth, uploader.single("note_file"), NoteCTRL.add);

router.route("/userfetch").get(token_auth, NoteCTRL.fetchdash);
router.route("/search").get(NoteCTRL.search);

router
  .route("/:id")
  .get(NoteCTRL.fetchSingle)
  .put(token_auth, uploader.single("note_file"), NoteCTRL.update)
  .delete(token_auth, NoteCTRL.remove);

module.exports = router;
