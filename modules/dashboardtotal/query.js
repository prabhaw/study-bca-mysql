const db = require("./../../db");

function totalblog(condition) {
  return new Promise(function (resolve, reject) {
    db.blogs
      .count({ where: condition })
      .then(function (total) {
        resolve(total);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function totalnote(condition) {
  return new Promise(function (resolve, reject) {
    db.notes
      .count({ where: condition })
      .then(function (total) {
        resolve(total);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function totalquestion(condition) {
  return new Promise(function (resolve, reject) {
    db.question
      .count({ where: condition })
      .then(function (total) {
        resolve(total);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

module.exports = { totalblog, totalnote, totalquestion };
