const TotalQuery = require("./query");

function blogtotal(req, res, next) {
  const condition = {};
  condition.user_id = req.loggedInUser.id;
  TotalQuery.totalblog(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error On Geting Total Blog." });
    });
}

function notetotal(req, res, next) {
  const condition = {};
  condition.user_id = req.loggedInUser.id;
  TotalQuery.totalnote(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error on Geting Total Note" });
    });
}

function questiontotal(req, res, next) {
  const condition = {};
  condition.user_id = req.loggedInUser.id;
  TotalQuery.totalquestion(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error on Geting Total Questiion." });
    });
}

module.exports = { blogtotal, notetotal, questiontotal };
