const router = require("express").Router();
const Total = require("./controller");
const token_auth = require("./../../middleware/token_authentication");

router.route("/blog").get(token_auth, Total.blogtotal);
router.route("/note").get(token_auth, Total.notetotal);
router.route("/question").get(token_auth, Total.questiontotal);

module.exports = router;
