const router = require("express").Router();
const BlogCTRL = require("./../controllers/blog.controller");
const token_auth = require("./../../../middleware/token_authentication");
const uploader = require("./../middlewares/upload");
router
  .route("/")
  .get(token_auth, BlogCTRL.fetchdash)
  .post(token_auth, uploader.single("blog_img"), BlogCTRL.add);
router.route("/adminsearch").get(token_auth, BlogCTRL.searchAdmin);
router.route("/publicfetch").get(BlogCTRL.publicfetch);
router.route("/publicsearch").get(BlogCTRL.searchPublic);
router.route("/publicblog/:id").get(BlogCTRL.fetchsingle);
router
  .route("/:id")
  .get(token_auth, BlogCTRL.fetchByIdDash)
  .put(token_auth, uploader.single("blog_img"), BlogCTRL.update)
  .delete(token_auth, BlogCTRL.remove);

module.exports = router;
