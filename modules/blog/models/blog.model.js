"use strict";
const moment = require("moment");

module.exports = function (sequelize, DataTypes) {
  const Blog = sequelize.define(
    "blog",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },

      user_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      title: {
        type: DataTypes.TEXT,
      },
      short_description: {
        type: DataTypes.STRING(225),
      },
      description: {
        type: DataTypes.TEXT,
      },
      blog_image: {
        type: DataTypes.STRING(225),
      },
      publish_date: {
        defaultValue: moment().format("MMM Do YYYY"),
        type: DataTypes.STRING(20),
      },
      verified: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
    },

    {
      underscored: true,
    }
  );
  return Blog;
};
