module.exports = function (blog, blogDetails) {
  if (blogDetails.user_id) {
    blog.user_id = blogDetails.user_id;
  }
  if (blogDetails.title) {
    blog.title = blogDetails.title;
  }
  if (blogDetails.short_description) {
    blog.short_description = blogDetails.short_description;
  }
  if (blogDetails.description) {
    blog.description = blogDetails.description;
  }
  if (blogDetails.category) {
    blog.category = blogDetails.category;
  }
  if (blogDetails.blog_image) {
    blog.blog_image = blogDetails.blog_image;
  }
  if (blogDetails.verified || blogDetails.verified == false) {
    blog.verified = blogDetails.verified;
  }
  return blog;
};
