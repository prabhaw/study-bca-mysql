const BlogQuery = require("./../query/blog.query");
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
function add(req, res, next) {
  if (req.fileErr) {
    return next({ mag: "Invalid File Format.", status: 400 });
  }

  var data = req.body;
  data.user_id = req.loggedInUser.id;
  if (req.file) {
    data.blog_image = req.file.filename;
  }

  BlogQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error on Adding Blog" });
    });
}

function fetchdash(req, res, next) {
  var condition = {};
  if (req.loggedInUser.role != 1) {
    condition.user_id = req.loggedInUser.id;
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  BlogQuery.find(condition, {}, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "ERROR whil Fetching Blog." });
    });
}

function fetchByIdDash(req, res, next) {
  var condition = {
    id: req.params.id,
  };
  if (req.loggedInUser.role != 1) {
    condition.user_id = req.loggedInUser.id;
  }
  BlogQuery.findOne(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Blog." });
    });
}

function update(req, res, next) {
  var id = req.params.id;
  const data = req.body;
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }
  if (req.file) {
    data.blog_image = req.file.filename;
  }

  BlogQuery.update(id, data)

    .then(function (blog) {
      res.status(200).json(blog.data);
      if (req.file && blog.oldImage) {
        fs.unlink(
          path.join(process.cwd(), "uploads/blog_image/" + blog.oldImage),
          function (err, done) {
            if (err) {
              console.log("Error When Removing File.");
            } else {
              console.log("Removed Successfully.");
            }
          }
        );
      }
    })
    .catch(function (err) {

      next(err.msg || { msg: "Error While Updating Blog." });
    });
}

function remove(req, res, next) {
  BlogQuery.remove(req.params.id)
    .then(function (data) {
      res.status(200).json({ msg: "Successfully Blog Deleted." });
      if (data.Image) {
        fs.unlink(
          path.join(process.cwd(), "uploads/blog_image/" + data.Image),
          function (err, done) {
            if (err) {
              console.log("Error while removeing Image.");
            } else {
              console.log("Remove Successfully.");
            }
          }
        );
      }
    })
    .catch(function (err) {

      next(err.msg || { msg: "Error While Removing Blog." });
    });
}

function searchAdmin(req, res, next) {
  const search_value = req.query.search;
  const condition = {};
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  if (req.loggedInUser.role == 1) {
    var mapSearch = {};

    if (search_value) {
      mapSearch = {
        [Op.or]: [{ title: { [Op.like]: "%" + search_value + "%" } }],
      };
    } else {
      mapSearch = condition;
    }
    BlogQuery.find(mapSearch, {}, pageOptions)
      .then(function (data) {
        res.status(200).json(data);
      })
      .catch(function (err) {

        next({ msg: "ERROR While Searching Data" });
      });
  } else {
    next({ msg: "You are not Administrator." });
  }
}

function publicfetch(req, res, next) {
  var condition = { verified: true };

  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  BlogQuery.find(condition, { active: true, verify: true }, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "ERROR whil Fetching Blog." });
    });
}

function searchPublic(req, res, next) {
  const search_value = req.query.search;
  const condition = { verified: true };
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  var mapSearch = {};

  if (search_value) {
    mapSearch = {
      [Op.and]: [
        {
          [Op.or]: [
            { title: { [Op.like]: "%" + search_value + "%" } },
            {
              short_description: {
                [Op.like]: "%" + " " + search_value + " " + "%",
              },
            },
          ],
        },
        condition,
      ],
    };
  } else {
    mapSearch = condition;
  }
  BlogQuery.find(mapSearch, { active: true, verify: true }, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      
      next({ msg: "ERROR While Searching Data" });
    });
}

function fetchsingle(req, res, next) {
  var condition = {
    [Op.and]: [{ id: req.params.id }, { verified: true }],
  };

  BlogQuery.findOne(condition, { active: true, verify: true })
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {

      next({ msg: "Error While Fetching Blog." });
    });
}

module.exports = {
  add,
  fetchdash,
  fetchByIdDash,
  update,
  remove,
  searchAdmin,
  publicfetch,
  searchPublic,
  fetchsingle,
};
