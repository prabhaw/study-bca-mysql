const db = require("./../../../db");
const map_blog_data = require("./../helper/map_blog");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newBlog = {};
    var newMappedBlog = map_blog_data(newBlog, data);
    db.blogs
      .create(newMappedBlog)
      .then(function (data) {
        db.blogs
          .findOne({
            where: { id: data.id },
            include: [
              {
                model: db.users,
                attributes: [
                  "first_name",
                  "last_name",
                  "email",
                  "role",
                  "description",
                  "verify",
                  "profile_image",
                  "university",
                  "college",
                  "contact",
                  "active",
                ],
              },
            ],
          })
          .then(function (data) {
            resolve(data);
          })
          .catch(function (err) {
            reject(err);
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function find(condition, subcondition = {}, option = {}) {
  return new Promise(function (resolve, reject) {
    var limit = Number(option.pageSize) || 12;
    var page = Number(option.pageNumber) || 1;

    db.blogs
      .findAll({
        where: condition,
        offset: (page - 1) * limit,
        limit: limit,
        order: [["created_at", "DESC"]],
        include: [
          {
            model: db.users,
            where: subcondition,
            attributes: [
              "first_name",
              "last_name",
              "email",
              "role",
              "description",
              "verify",
              "profile_image",
              "university",
              "college",
              "contact",
              "active",
            ],
          },
        ],
      })
      .then(function (data) {
        db.blogs
          .count({
            where: condition,
            include: [
              {
                model: db.users,
                where: subcondition,
              },
            ],
          })
          .then(function (total) {
            resolve({ data, total });
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function findOne(condition, subcondition = {}) {
  return db.blogs.findOne({
    where: condition,
    include: [
      {
        model: db.users,
        where: subcondition,
        attributes: [
          "first_name",
          "last_name",
          "email",
          "role",
          "description",
          "verify",
          "profile_image",
          "university",
          "college",
          "contact",
        ],
      },
    ],
  });
}

function update(id, data) {
  return new Promise(function (resolve, reject) {
    db.blogs.findByPk(id).then(function (blog) {
      if (blog) {
        var oldImage = blog.blog_image;
        var updatedMappedUser = map_blog_data({}, data);

        db.blogs
          .update(updatedMappedUser, { where: { id: id } })
          .then(function (num) {
            if (num == 1) {
              db.blogs
                .findOne({
                  where: { id: blog.id },
                  include: [
                    {
                      model: db.users,
                      attributes: [
                        "first_name",
                        "last_name",
                        "email",
                        "role",
                        "description",
                        "verify",
                        "profile_image",
                        "university",
                        "college",
                        "contact",
                        "active",
                      ],
                    },
                  ],
                })
                .then(function (data) {
                  resolve({ data, oldImage });
                })
                .catch(function (err) {
                  reject(err);
                });
            } else {
              reject({ msg: "Blog Not Found", status: 404 });
            }
          });
      }
    });
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.blogs
      .findByPk(id)
      .then(function (blog) {
        if (blog) {
          var Image = blog.blog_image;
          db.blogs.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve({ Image });
            } else {
              reject();
            }
          });
        } else {
          reject({ msg: "Blog Not Found." });
        }
      })
      .catch(function (err) {
        reject({ msg: "Blog Was Not Found" });
      });
  });
}

module.exports = { insert, find, update, remove, findOne };
