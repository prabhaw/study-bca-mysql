const db = require("./../../../db");
const map_slider_data = require("./../helper/map_slider");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newSlider = {};
    var newMappedSlider = map_slider_data(newSlider, data);

    db.slider
      .create(newMappedSlider)
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function find(condition) {
  return new Promise(function (resolve, reject) {
    db.slider
      .findAll({ where: condition })

      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.slider
      .findByPk(id)
      .then(function (slider) {
        if (slider) {
          var File = slider.slider_img;
          db.slider.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve({ File });
            } else {
              reject();
            }
          });
        } else {
          reject({ msg: "Note Not Found." });
        }
      })
      .catch(function (err) {
        reject({ msg: "Note Was Not Found" });
      });
  });
}

module.exports = { insert, find, remove };
