const router = require("express").Router();
const SlideCTRL = require("./../controllers/slider.controller");
const token_auth = require("./../../../middleware/token_authentication");
const uploader = require("./../middlewares/upload");

router
  .route("/")
  .get(SlideCTRL.fetch)
  .post(token_auth, uploader.single("slider_img"), SlideCTRL.add);
router.route("/:id").delete(token_auth, SlideCTRL.remove);

module.exports = router;
