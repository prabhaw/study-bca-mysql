"use strict";

module.exports = function (sequelize, DataTypes) {
  const Slider = sequelize.define(
    "slider",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },
      slider_img: {
        type: DataTypes.TEXT,
      },
    },
    {
      underscored: true,
    }
  );
  return Slider;
};
