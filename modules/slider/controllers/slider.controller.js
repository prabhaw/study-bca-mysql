const SliderQuery = require("./../query/slider.query");
const fs = require("fs");
const path = require("path");

function add(req, res, next) {
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }

  var data = req.body;
  if (req.file) {
    data.slider_img = req.file.filename;
  }

  SliderQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .then(function (err) {
      next({ msg: "Error On Adding Note. " });
    });
}

function fetch(req, res, next) {
  var condition = {};
  SliderQuery.find(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetcing SLider." });
    });
}

function remove(req, res, next) {
  SliderQuery.remove(req.params.id)
    .then(function (data) {
      res.status(200).json({ msg: "Successfully  Deleted. " });
      if (data.File) {
        fs.unlink(
          path.join(process.cwd(), "uploads/slider/" + data.File),
          function (err, done) {
            if (err) {
              console.log("Error while removeing File.");
            } else {
              console.log("Remove Successfully.");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Removing Note." });
    });
}

module.exports = {
  add,
  fetch,
  remove,
};
