module.exports = function (slider, sliderDetails) {
  if (sliderDetails.slider_img) {
    slider.slider_img = sliderDetails.slider_img;
  }
  return slider;
};
