const CollegeQuery = require("./../query/college.query");
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

function add(req, res, next) {
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }
  var data = req.body;
  if (req.file) {
    data.college_image = req.file.filename;
  }
  CollegeQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      console.log(err);
      next(err.msg || { msg: "Error While Adding College." });
    });
}

function fetchByAdmin(req, res, next) {
  var condition = {};
  if (req.loggedInUser.role == 1) {
    var pageOption = {};
    pageOption.pageSize = req.query.pageSize;
    pageOption.pageNumber = req.query.pageNumber;
    CollegeQuery.find(condition, pageOption)
      .then(function (data) {
        res.status(200).json(data);
      })
      .catch(function (err) {
        console.log(err);
        next(err.msg || { msg: "Error While Fetching Data" });
      });
  } else {
    next({ msg: "Invalid User." });
  }
}

function publicfetch(req, res, next) {
  var condition = { verified: true };
  var pageOptions = {};

  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;
  CollegeQuery.find(condition, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Colleges." });
    });
}

function topfetch(req, res, next) {
  var condition = { verified: true, top_college: true };

  CollegeQuery.topfind(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Colleges." });
    });
}

function fetchsingel(req, res, next) {
  var condition = {
    [Op.and]: [{ id: req.params.id }, { verified: true }],
  };
  CollegeQuery.findOne(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetchig College Data." });
    });
}

function update(req, res, next) {
  var id = req.params.id;
  const data = req.body;

  if (req.loggedInUser.role == 1) {
    CollegeQuery.update(id, data)
      .then(function (college) {
        res.status(200).json(college);
      })
      .catch(function (err) {
        next(err.msg || { msg: "Error While Updating College" });
      });
  }
}

function remove(req, res, next) {
  CollegeQuery.remove(req.params.id)
    .then(function (data) {
      res.status(200).json({ msg: "Successfully Deleted College." });
      if (data.Image) {
        fs.unlink(
          path.join(process.cwd(), "uploads/college_image/" + data.Image),
          function (err, done) {
            if (err) {
              console.log("Error While Removing Image.");
            } else {
              console.log("Remove Successfully.");
            }
          }
        );
      }
    })
    .catch(function (err) {
      next(err.msg || { msg: "Error While Removing College." });
    });
}

function searchAdmin(req, res, next) {
  const search_value = req.query.search;

  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;
  if (req.loggedInUser.role == 1) {
    var mapSearch = {};
    if (search_value) {
      mapSearch = {
        [Op.or]: [
          { college_name: { [Op.like]: "%" + search_value + "%" } },
          { university: { [Op.like]: "%" + search_value + "%" } },
          { address: { [Op.like]: "%" + search_value + "%" } },
        ],
      };
    }
    CollegeQuery.find(mapSearch, pageOptions)
      .then(function (data) {
        res.status(200).json(data);
      })
      .catch(function (err) {
        next({ msg: "Error While Geting Data." });
      });
  } else {
    next({ msg: "You are not Admin." });
  }
}

function searchPublic(req, res, next) {
  const search_value = req.query.search;
  const condition = { verified: true };
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  var mapSearch = {};
  if (search_value) {
    mapSearch = {
      [Op.and]: [
        {
          [Op.or]: [
            { college_name: { [Op.like]: "%" + search_value + "%" } },
            { university: { [Op.like]: "%" + search_value + "%" } },
            { address: { [Op.like]: "%" + search_value + "%" } },
          ],
        },
        condition,
      ],
    };
  } else {
    mapSearch = condition;
  }
  CollegeQuery.find(mapSearch, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Geting Data." });
    });
}

module.exports = {
  add,
  fetchByAdmin,
  publicfetch,
  fetchsingel,
  update,
  remove,
  searchAdmin,
  searchPublic,
  topfetch,
};
