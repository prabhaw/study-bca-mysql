module.exports = function (college, collegeDetail) {
  if (collegeDetail.college_name) {
    college.college_name = collegeDetail.college_name;
  }
  if (collegeDetail.university) {
    college.university = collegeDetail.university;
  }
  if (collegeDetail.address) {
    college.address = collegeDetail.address;
  }
  if (collegeDetail.college_image) {
    college.college_image = collegeDetail.college_image;
  }
  if (collegeDetail.description) {
    college.description = collegeDetail.description;
  }
  if (collegeDetail.verified || collegeDetail.verified == false) {
    college.verified = collegeDetail.verified;
  }
  if (collegeDetail.top_college || collegeDetail.top_college == false) {
    college.top_college = collegeDetail.top_college;
  }
  return college;
};
