const router = require("express").Router();
const CollegeCTRL = require("../controllers/college.controller");
const token_auth = require("../../../middleware/token_authentication");
const uploader = require("../middlewares/upload");

router
  .route("/")
  .get(CollegeCTRL.publicfetch)
  .post(uploader.single("college_image"), CollegeCTRL.add);
router.route("/adminfetch").get(token_auth, CollegeCTRL.fetchByAdmin);
router.route("/adminsearch").get(token_auth, CollegeCTRL.searchAdmin);
router.route("/publicsearch").get(CollegeCTRL.searchPublic);
router.route("/topcollege").get(CollegeCTRL.topfetch);
router
  .route("/:id")
  .get(CollegeCTRL.fetchsingel)
  .put(token_auth, CollegeCTRL.update)
  .delete(token_auth, CollegeCTRL.remove);

module.exports = router;
