const db = require("./../../../db");
const map_college_data = require("./../helper/map_college");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newCollege = {};
    var newMappedCollege = map_college_data(newCollege, data);
    db.colleges
      .create(newMappedCollege)
      .then(function (data) {
        resolve({ msg: "College Has Been Added" });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function find(condition, option = {}) {
  return new Promise(function (resolve, reject) {
    var limit = Number(option.pageSize) || 12;
    var page = Number(option.pageNumber) || 1;
    db.colleges
      .findAll({
        where: condition,
        offset: (page - 1) * limit,
        limit: limit,
        order: [["created_at", "ASC"]],
      })
      .then(function (data) {
        db.colleges.count({ where: condition }).then(function (total) {
          resolve({ data, total });
        });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function topfind(condition) {
  return new Promise(function (resolve, reject) {
    db.colleges
      .findAll({
        where: condition,
        order: [["created_at", "ASC"]],
      })
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function findOne(condition) {
  return db.colleges.findOne({
    where: condition,
  });
}

function update(id, data) {
  return new Promise(function (resolve, reject) {
    db.colleges
      .findByPk(id)
      .then(function (college) {
        if (college) {
          var updatedMappedUser = map_college_data({}, data);
          db.colleges
            .update(updatedMappedUser, { where: { id: id } })
            .then(function (num) {
              if (num == 1) {
                resolve({ msg: "College is Updated.", status: 200 });
              }
            });
        } else {
          reject({ msg: "College Not Found", status: 404 });
        }
      })
      .catch(function (err) {
        reject({ msg: "College Not Found", status: 404 });
      });
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.colleges
      .findByPk(id)
      .then(function (college) {
        if (college) {
          var Image = college.college_image;
          db.colleges.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve({ Image });
            } else {
              reject();
            }
          });
        } else {
          reject({ msg: "College Not Found." });
        }
      })
      .catch(function (err) {
        reject({ msg: "College Is Not Deleted." });
      });
  });
}

module.exports = { insert, find, update, remove, findOne, topfind };
