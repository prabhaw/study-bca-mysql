const AuthCTRL = require("./../controllers/auth.controller");
const router = require("express").Router();

router.route("/").post(AuthCTRL.logIn);

module.exports = router;
