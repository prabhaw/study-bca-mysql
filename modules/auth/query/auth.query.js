const AuthModel = require("./../../../db").users;

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const JWT_secret = process.env.JWT_secret;

function createToken(data) {
  var token = jwt.sign(
    {
      id: data.id,
    },
    JWT_secret
  );
  return token;
}

function login(data) {
  return new Promise(function (resolve, reject) {
    AuthModel.findOne({
      attributes: [
        "id",
        "first_name",
        "last_name",
        "password",
        "email",
        "role",
        "description",
        "verify",
        "profile_image",
        "university",
        "college",
        "contact",
      ],
      where: {
        email: data.username,
      },
    })
      .then(function (user) {
        if (user) {
          bcrypt.compare(data.password, user.password, function (err, isMatch) {
            if (isMatch) {
              return resolve({ user, token: createToken(user) });
            } else {
              return reject({ msg: "Invalid Username Or Password" });
            }
          });
        } else {
          return reject({ msg: "Invalid Username Or Password" });
        }
      })
      .catch(function (err) {
        reject({ msg: "Error while SignIn." });
      });
  });
}

module.exports = {
  login,
};
