const AuthQuery = require("./../query/auth.query");
const AuthModel = require("./../../user/models/user.model");
function logIn(req, res, next) {
  AuthQuery.login(req.body)
    .then(function (done) {
      res.status(200).json(done);
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = { logIn };
