"use strict";

module.exports = function (sequelize, DataTypes) {
  const Syllabus = sequelize.define(
    "syllabus",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },
      university: {
        type: DataTypes.STRING(225),
        allowNull: false,
        set(val) {
          this.setDataValue("university", val.toUpperCase().trim());
        },
      },
      semester: {
        type: DataTypes.STRING(225),
        allowNull: false,
        set(val) {
          this.setDataValue("semester", val.toUpperCase().trim());
        },
      },
      subject: {
        type: DataTypes.STRING(225),
        allowNull: false,
        set(val) {
          this.setDataValue("subject", val.toUpperCase().trim());
        },
      },
      syllabus: {
        type: DataTypes.TEXT,
      },
      credit_hrs: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      course_code: {
        type: DataTypes.STRING(50),
        allowNull: false,
        set(val) {
          this.setDataValue("course_code", val.toUpperCase().trim());
        },
      },
    },
    {
      underscored: true,
    }
  );
  return Syllabus;
};
