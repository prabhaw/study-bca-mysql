const router = require("express").Router();
const SyllabusCTRL = require("./../controllers/syllabus.controller");
const token_auth = require("./../../../middleware/token_authentication");
const uploader = require("./../middlewares/upload");

router
  .route("/")
  .get(SyllabusCTRL.fetch)
  .post(token_auth, uploader.single("syllabus"), SyllabusCTRL.add);
router.route("/input").get(SyllabusCTRL.fetchforInput);
router.route("/search").get(SyllabusCTRL.search);
router.route("/:id").delete(token_auth, SyllabusCTRL.remove);

module.exports = router;
