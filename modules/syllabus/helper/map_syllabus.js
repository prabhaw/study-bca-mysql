module.exports = function (syllabus, syllabusDetails) {
  if (syllabusDetails.university) {
    syllabus.university = syllabusDetails.university;
  }
  if (syllabusDetails.semester) {
    syllabus.semester = syllabusDetails.semester;
  }
  if (syllabusDetails.subject) {
    syllabus.subject = syllabusDetails.subject;
  }
  if (syllabusDetails.syllabus) {
    syllabus.syllabus = syllabusDetails.syllabus;
  }
  if (syllabusDetails.credit_hrs) {
    syllabus.credit_hrs = syllabusDetails.credit_hrs;
  }
  if (syllabusDetails.course_code) {
    syllabus.course_code = syllabusDetails.course_code;
  }
  return syllabus;
};
