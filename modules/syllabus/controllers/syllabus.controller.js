const SyllabusQuery = require("./../query/syllabus.query");
const fs = require("fs");
const path = require("path");
const blogQuery = require("../../blog/query/blog.query");
const { features } = require("process");

function add(req, res, next) {
  if (req.fileErr) {
    return next({ msg: "Invalid File Format.", status: 400 });
  }
  var data = req.body;
  if (req.file) {
    data.syllabus = req.file.filename;
  }
  SyllabusQuery.insert(data)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      console.log(err);
      next({ msg: "Error On Adding Syllabus." });
    });
}

function fetch(req, res, next) {
  var condition = {};
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;
  SyllabusQuery.find(condition, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Fetching Syllabus." });
    });
}

function search(req, res, next) {
  const condition = {};
  const university = req.query.university;
  const semester = req.query.semester;
  const subject = req.query.subject;
  if (university) {
    condition.university = university;
  }
  if (semester) {
    condition.semester = semester;
  }
  if (subject) {
    condition.subject = subject;
  }
  var pageOptions = {};
  pageOptions.pageSize = req.query.pageSize;
  pageOptions.pageNumber = req.query.pageNumber;

  SyllabusQuery.find(condition, pageOptions)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error While Searching Syllabus." });
    });
}
function fetchforInput(req, res, next) {
  const university = req.query.university;
  const semester = req.query.semester;

  const condition = {};

  if (university) {
    condition.university = university;
  }
  if (semester) {
    condition.semester = semester;
  }

  SyllabusQuery.findsearch(condition)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next({ msg: "Error on Data." });
    });
}
function remove(req, res, next) {
  if (req.loggedInUser.role == 1) {
    SyllabusQuery.remove(req.params.id)
      .then(function (data) {
        res.status(200).json({ msg: "Successfully Deleted." });
        if (data.syllabus) {
          fs.unlink(
            path.join(process.cwd(), "uploads/syllabus/" + data.syllabus),
            function (err, done) {
              if (err) {
                console.log("Error While Remving File");
              } else {
                console.log("File Remove Successfully.");
              }
            }
          );
        }
      })
      .catch(function (err) {
        next(err.msg || { msg: "Error While Removing Syllabus." });
      });
  } else {
    next({ msg: "You are not Administrator." });
  }
}

module.exports = { add, fetch, search, remove, fetchforInput };
