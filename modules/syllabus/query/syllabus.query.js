const db = require("./../../../db");
const map_syllabus_data = require("./../helper/map_syllabus");

function insert(data) {
  return new Promise(function (resolve, reject) {
    var newSyllabus = {};
    var newMappedSyllabus = map_syllabus_data(newSyllabus, data);
    db.syllabus
      .create(newMappedSyllabus)
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function find(condition, option = {}) {
  return new Promise(function (resolve, reject) {
    var limit = Number(option.pageSize) || 12;
    var page = Number(option.pageNumber) || 1;
    db.syllabus
      .findAll({
        where: condition,
        offset: (page - 1) * limit,
        limit: limit,
        order: [["semester", "ASC"]],
      })
      .then(function (data) {
        db.syllabus.count({ where: condition }).then(function (total) {
          resolve({ data, total });
        });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function findsearch(condition) {
  return new Promise(function (resolve, reject) {
    db.syllabus
      .findAll({
        where: condition,
        order: [["semester", "ASC"]],
        attributes: ["university", "semester", "subject"],
      })
      .then(function (data) {
        resolve(data);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

function remove(id) {
  return new Promise(function (resolve, reject) {
    db.syllabus
      .findByPk(id)
      .then(function (blog) {
        if (blog) {
          var syllabus = blog.syllabus;
          db.syllabus.destroy({ where: { id: id } }).then(function (num) {
            if (num == 1) {
              resolve({ syllabus });
            } else {
              reject();
            }
          });
        } else {
          reject({ msg: "Syllabus not Found." });
        }
      })
      .catch(function (err) {
        reject({ msg: "Syllabus Not Found." });
      });
  });
}

module.exports = { insert, find, findsearch, remove };
