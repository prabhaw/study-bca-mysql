"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("users", {
      id: {
        defaultValue: Sequelize.UUID,
        type: Sequelize.UUID,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },
      first_name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      last_name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },

      password: {
        type: Sequelize.STRING(255),
      },
      email: {
        type: Sequelize.STRING(50),
        allowNull: false,

        unique: true,
      },
      role: {
        defaultValue: 0,
        type: Sequelize.INTEGER(2),
      },
      description: {
        type: Sequelize.TEXT,
      },
      verify: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      verifycode: {
        type: Sequelize.STRING(225),
        defaultValue: null,
      },
      contact: {
        type: Sequelize.STRING(10),
      },
      university: {
        type: Sequelize.STRING(225),
      },
      college: {
        type: Sequelize.STRING(225),
      },
      verifyCodeExpiry: Sequelize.DATE,
      passwordResetToken: Sequelize.STRING(200),
      passwordResetTokenExpiry: Sequelize.DATE,
      profile_image: Sequelize.TEXT,

      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
  return  queryInterface.dropTable("users");
  },
};
