"use strict";

module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable(
      "oldquestions",
      {
        id: {
          type: DataTypes.UUID,

          allowNull: false,
          unique: true,
          primaryKey: true,
        },
        user_id: {
          type: DataTypes.UUID,
          allowNull: false,
        },
        subject: {
          type: DataTypes.STRING(225),
        },
        university: {
          type: DataTypes.STRING(225),
        },
        semester: {
          type: DataTypes.STRING(225),
        },
        year: {
          type: DataTypes.INTEGER(4),
        },
        exam_time: {
          type: DataTypes.STRING(50),
        },
        old_question: {
          type: DataTypes.TEXT,
        },
        publish_date: {
          type: DataTypes.STRING(20),
        },
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
      },
      {
        underscored: true,
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("oldquestions");
  },
};
