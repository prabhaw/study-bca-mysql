"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "books",
      {
        id: {
          type: Sequelize.UUID,
          allowNull: false,
          unique: true,
          primaryKey: true,
        },
        university: {
          type: Sequelize.STRING(225),
          allowNull: false,
        },
        semester: { type: Sequelize.STRING(225), allowNull: false },
        subject: { type: Sequelize.STRING(225), allowNull: false },
        name: { type: Sequelize.STRING(225), allowNull: false },
        author: { type: Sequelize.STRING(225), allowNull: false },
        publication: { type: Sequelize.STRING(225), allowNull: false },
        book_img: { type: Sequelize.STRING(225), allowNull: false },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
      },
      {
        underscored: true,
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("books");
  },
};
