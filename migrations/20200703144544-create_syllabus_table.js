"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("syllabuses", {
      id: {
        type: Sequelize.UUID,

        allowNull: false,
        unique: true,
        primaryKey: true,
      },
      university: {
        type: Sequelize.STRING(225),
        allowNull: false,
      },
      semester: {
        type: Sequelize.STRING(225),
        allowNull: false,
      },
      subject: {
        type: Sequelize.STRING(225),
        allowNull: false,
      },
      credit_hrs: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      course_code: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      syllabus: {
        type: Sequelize.TEXT,
      },

      created_at: Sequelize.DATE,
      updated_at: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("syllabuses");
  },
};
