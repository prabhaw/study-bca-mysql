"use strict";

module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable(
      "notes",
      {
        id: {
          type: DataTypes.UUID,

          allowNull: false,
          unique: true,
          primaryKey: true,
        },
        user_id: {
          type: DataTypes.UUID,
          allowNull: false,
        },
        subject: {
          type: DataTypes.STRING(225),
        },
        university: {
          type: DataTypes.STRING(225),
        },
        semester: {
          type: DataTypes.STRING(225),
        },
        note: {
          type: DataTypes.TEXT,
        },
        publish_date: {
          type: DataTypes.STRING(20),
        },
        verified: {
          type: DataTypes.BOOLEAN,
          defaultValue: true,
        },
        description: {
          type: DataTypes.TEXT,
        },
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
      },
      {
        underscored: true,
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("notes");
  },
};
