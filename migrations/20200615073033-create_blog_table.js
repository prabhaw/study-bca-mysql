module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("blogs", {
      id: {
        defaultValue: Sequelize.UUID,
        type: Sequelize.UUID,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },

      user_id: {
        type: Sequelize.UUID,
        onDelete: "cascade",
        references: {
          model: "users",
          key: "id",
        },
      },
      title: {
        type: Sequelize.TEXT,
      },
      short_description: {
        type: Sequelize.TEXT,
      },
      description: {
        type: Sequelize.TEXT,
      },

      blog_image: {
        type: Sequelize.STRING(225),
      },
      publish_date: {
        type: Sequelize.STRING(20),
      },
      verified: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      created_at: Sequelize.DATE,
      updated_at: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
  return  queryInterface.dropTable("blogs");
  },
};
